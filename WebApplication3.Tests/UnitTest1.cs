using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApplication3.Controllers;

namespace WebApplication3.Tests
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void CanGetIndex()
        {
            var controller = new HomeController();
            var result = controller.Index();
            Assert.IsNotNull(result);
        }
    }
}
